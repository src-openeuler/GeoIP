%bcond_with tests

Name:           GeoIP
Version:        1.6.12
Release:        6
Summary:        Library for country/city/organization to IP address or hostname mapping
License:        LGPLv2+
URL:            https://dev.maxmind.com/geoip/legacy/downloadable/
Source0:        https://github.com/maxmind/geoip-api-c/releases/download/v%{version}/GeoIP-%{version}.tar.gz


BuildRequires:  coreutils  gcc make sed zlib-devel
Requires:       GeoIP-data

Obsoletes:      geoip < %{version}-%{release}
Provides:       geoip = %{version}-%{release}

%description
The GeoIP Legacy C library enables the user to find geographical and network information of
an IP address. To use this library, you may user our commercial GeoIP Legacy databases.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Provides:       geoip-devel = %{version}-%{release}
Obsoletes:      geoip-devel < %{version}-%{release}

%description devel
Development-relative files for including static libraries, header,example,test use cases
and other information for build %{name}-based applications.



%package        help
Summary:        Documents for autogen
Buildarch:      noarch
Requires:       man

%description    help
Man pages and other related documents for GeoIP.



%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static --disable-dependency-tracking
sed -i -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
%make_install

%delete_la

%check
%{?with_tests:LD_LIBRARY_PATH=%{buildroot}%{_libdir} make check}



%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%files
%defattr(-,root,root)
%if 0%{?_licensedir:1}
%license COPYING
%else
%doc COPYING
%endif
%doc AUTHORS ChangeLog NEWS.md README.md
%{_bindir}/{geoiplookup,geoiplookup6}
%{_libdir}/{libGeoIP.so.1,libGeoIP.so.1.*}


%files devel
%{_includedir}/{GeoIP.h,GeoIPCity.h}
%{_libdir}/libGeoIP.so
%{_libdir}/pkgconfig/geoip.pc


%files help
%{_mandir}/man1/{geoiplookup.1*,geoiplookup6.1*}


%changelog
* Tue Dec 15 2020 xihaochen <xihaochen@huawei.com> - 1.6.12-6
- Type:requirement
- Id:NA
- SUG:NA
- DESC:update url

* Wed  Sep 25 2019 openEuler Cynthia Xie <xiehong@huawei.com> - 1.6.12-5
- Package init



